#!/usr/bin/env sh
#
# Copyright 2022 Tymoteusz Blazejczyk
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit on error
set -e

workdir="$(pwd)"
user_id="$(id -u)"
group_id="$(id -g)"

# Run Docker image as container. Mount current working directory to container.
# This will allow all commands from Docker to have access to files and
# directories under current working directory, mostly of time it is a project
# workspace. All created files and directories will have proper mode file
# permissions from current user who invokes this script
docker run \
    --rm \
    --tty \
    --interactive \
    --user "${user_id}:${group_id}" \
    --volume "${workdir}:${workdir}:ro,z" \
    --workdir "${workdir}" \
    "registry.gitlab.com/tymonx/gitlab-ci/docker:${GITLAB_CI_DOCKER_TAG:-1.11.1}" \
    "$@"
