# GitLab CI

Various useful and handy GitLab CI YAML templates for typical GitLab CI tasks.

[[_TOC_]]

## Features

* Supporting validating, building, testing and auto-documenting projects out-of-box
* Automating various and mostly typical GitLab CI tasks
* Easy to use with the GitLab CI `include` keyword in your `.gitlab-ci.yml` files
* Pre-built Docker images with all required tools for GitLab CI that can be run locally

## Usage

### Include

```yaml
include:
    - project: 'tymonx/gitlab-ci'
      ref: v1.11.2
      file: '/templates.yml'
```

### Go

```yaml
include:
    - project: 'tymonx/gitlab-ci'
      ref: v1.11.2
      file: '/templates.yml'

# Building Go source files
go-build:
    extends: .go-build

# Validating source file format, imports, errors and linting Go source files
go-lint:
    extends: .go-lint

# Running tests, validating coverage and generating test and coverage reports
go-test:
    extends: .go-test

# Checking licences for built Go binaries
go-license:
    extends: .go-license

# Genering HTML source code documentation and HTML coverage report. Upload them
# to GitLab Pages site for browsing. Dependencies is needed to download
# generated coverage report artifact from previous test job
pages:
    extends: .go-doc
    dependencies:
        - test
```

### Docker

```yaml
include:
    - project: 'tymonx/gitlab-ci'
      ref: v1.11.2
      file: '/templates.yml'

# It builds Docker image
docker-build:
    extends: .docker-build

# It lints Dockerfiles
docker-lint:
    extends: .docker-lint

# It tests built Docker image
docker-test:
    extends: .docker-test
    script:
        - <run-my-tests-inside-docker>

# It deploys edge image to registry
docker-edge:
    extends: .docker-edge

# It deploys latest image to registry
docker-latest:
    extends: .docker-latest

# It deploys release image to registry
docker-deploy:
    extends: .docker-deploy
```

### Shell

```yaml
include:
    - project: 'tymonx/gitlab-ci'
      ref: v1.11.2
      file: '/templates.yml'

shell-lint:
    extends: .shell-lint
```

### Markdown

```yaml
include:
    - project: 'tymonx/gitlab-ci'
      ref: v1.11.2
      file: '/templates.yml'

markdown-lint:
    extends: .markdown-lint
```

### YAML

```yaml
include:
    - project: 'tymonx/gitlab-ci'
      ref: v1.11.2
      file: '/templates.yml'

yaml-lint:
    extends: .yaml-lint
```

### JSON

```yaml
include:
    - project: 'tymonx/gitlab-ci'
      ref: v1.11.2
      file: '/templates.yml'

json-lint:
    extends: .json-lint
```

### Workspace

```yaml
include:
    - project: 'tymonx/gitlab-ci'
      ref: v1.11.2
      file: '/templates.yml'

workspace-lint:
    extends: .workspace-lint
```

### License

```yaml
include:
    - project: 'tymonx/gitlab-ci'
      ref: v1.11.2
      file: '/templates.yml'

license-lint:
    extends: .license-lint
```

### OpenAPI

```yaml
include:
    - project: 'tymonx/gitlab-ci'
      ref: v1.11.2
      file: '/templates.yml'

openapi-lint:
    extends: .openapi-lint

pages:
    extends: .openapi-doc
```

### Local development

To run all linter tools locally:

```plaintext
./scripts/lint
```

To run specific linter tools locally:

```plaintext
./scripts/lint yaml json markdown workspace license
```

To run specific tool locally:

```plaintext
./scripts/yaml
```

To run specific linter tool locally:

```plaintext
./scripts/yaml lint
```

To build all Docker images:

```plaintext
./scripts/build
```

To build specific Docker images:

```plaintext
./scripts/build yaml json markdown
```

## Documentation

GitLab CI YAML templates [documentation](https://gitlab.com/tymonx/gitlab-ci/-/wikis/Home).
