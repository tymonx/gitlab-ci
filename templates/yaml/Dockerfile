# Copyright 2022 Tymoteusz Blazejczyk
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

ARG ALPINE_IMAGE=alpine
ARG ALPINE_VERSION=3.15.0

ARG SHELLCHECK_IMAGE=koalaman/shellcheck
ARG SHELLCHECK_VERSION=v0.7.2

ARG HADOLINT_IMAGE=hadolint/hadolint
ARG HADOLINT_VERSION=2.8.0
ARG HADOLINT_TAG=$HADOLINT_VERSION-alpine

ARG DOCKERFILE_LINT_IMAGE=replicated/dockerfilelint
ARG DOCKERFILE_LINT_VERSION=60f6c68

ARG DOCKERFILE_LINTER_IMAGE=buddy/dockerfile-linter
ARG DOCKERFILE_LINTER_VERSION=1.1.5

ARG YAML_LINT_IMAGE=fleshgrinder/yamllint
ARG YAML_LINT_VERSION=1.26.3

ARG YQ_IMAGE=mikefarah/yq
ARG YQ_VERSION=4.16.1

# Run Dockerfile linter
FROM $HADOLINT_IMAGE:$HADOLINT_TAG
COPY Dockerfile /tmp/Dockerfile
RUN hadolint --failure-threshold ignore /tmp/Dockerfile

# Run Dockerfile linter
FROM $DOCKERFILE_LINT_IMAGE:$DOCKERFILE_LINT_VERSION
COPY Dockerfile /tmp/Dockerfile
RUN /dockerfilelint/bin/dockerfilelint /tmp/Dockerfile

# Run Dockerfile linter
FROM $DOCKERFILE_LINTER_IMAGE:$DOCKERFILE_LINTER_VERSION
COPY Dockerfile /tmp/Dockerfile
RUN /dockerfilelinter/bin/dockerfilelinter --error --ignore ER0012,ER0015 --file /tmp/Dockerfile

# Run YAML linter
FROM $YAML_LINT_IMAGE:$YAML_LINT_VERSION as yamllint
COPY base.yml /tmp/base.yml
COPY .yamllint.yml /usr/local/src/.yamllint.yml
RUN yamllint /tmp/base.yml

# Extract to shell script
FROM $YQ_IMAGE:$YQ_VERSION as builder
COPY base.yml /base.yml
# linter ignore=ER0001
RUN yq eval '.".yaml".script[1] | explode(.)' /base.yml > /workdir/entrypoint && chmod a+x /workdir/entrypoint

# Run shell script linter
FROM $SHELLCHECK_IMAGE:$SHELLCHECK_VERSION as shellcheck
FROM $ALPINE_IMAGE:$ALPINE_VERSION
COPY --from=shellcheck /bin/shellcheck /bin/shellcheck
COPY --from=builder /workdir/entrypoint /tmp/entrypoint
RUN shellcheck --enable=all --severity=style /tmp/entrypoint

# Build YAML image
FROM $ALPINE_IMAGE:$ALPINE_VERSION

COPY --from=builder /usr/bin/yq /usr/local/bin/yq
COPY --from=builder /workdir/entrypoint /usr/local/bin/entrypoint
COPY --from=yamllint /usr/local/bin/yamllint /usr/local/bin/yamllint

ENTRYPOINT ["/usr/local/bin/entrypoint"]
CMD ["yq"]
