# Copyright 2022 Tymoteusz Blazejczyk
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

---
.workspace-script: &workspace-script |-
    #!/usr/bin/env sh
    #
    # Copyright 2022 Tymoteusz Blazejczyk
    #
    # Licensed under the Apache License, Version 2.0 (the "License");
    # you may not use this file except in compliance with the License.
    # You may obtain a copy of the License at
    #
    #     http://www.apache.org/licenses/LICENSE-2.0
    #
    # Unless required by applicable law or agreed to in writing, software
    # distributed under the License is distributed on an "AS IS" BASIS,
    # WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    # See the License for the specific language governing permissions and
    # limitations under the License.

    # Check for command errors
    set -e

    # It returns project directory path
    get_project_dir() {
        if [ -n "${CI_PROJECT_DIR}" ]; then
            echo "${CI_PROJECT_DIR}"
            return
        fi

        if path="$(git rev-parse --show-toplevel 2>/dev/null)" && [ -d "${path}" ]; then
            echo "${path}"
            return
        fi

        pwd
    }

    # It strips variable from leading and trailing whitespaces
    strip() {
        sed -r -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//'
    }

    # It checks if contains true
    is_true() {
        test "$(echo "$1" | strip | grep -ciE '^(on|yes|true|enable|[1-9][0-9]*)$')" -ne 0
    }

    # It colors to green
    green() {
        printf '\e[1;32m%s\e[0m\n' "$*"
    }

    # It colors to red
    red() {
        printf '\e[1;31m%s\e[0m\n' "$*"
    }

    # It colors to cyan
    cyan() {
        printf '\e[1;36m%s\e[0m\n' "$*"
    }

    # It prints info text (cyan color)
    info() {
        cyan "$@"
    }

    # It prints error text (red color)
    error() {
        red "$@"
    }

    # It prints error text and exit (red color)
    fatal() {
        red "$@"
        exit 1
    }

    # It prints pass text (green color)
    pass() {
        green "$@"
    }

    # It prints fail text (red color)
    fail() {
        if is_true "${WORKSPACE_ALLOWED_TO_FAIL:-}"; then
            red "$@" " and allowed to fail. Shame on you!"
        else
            red "$@"
            WORKSPACE_FAILED=1
        fi
    }

    # It returns .gitignore
    get_getignore_file() {
        echo "$(get_project_dir)/.gitignore"
    }

    # It returns LICENSE file
    get_license_file() {
        output="$(find "$(get_project_dir)" -mindepth 1 -maxdepth 1 -type f -iname 'LICENSE' -o -iname 'LICENSE.*' | sort | head -n1)"
        echo "${output:-$(get_project_dir)/LICENSE}"
    }

    # It returns README file
    get_readme_file() {
        output="$(find "$(get_project_dir)" -mindepth 1 -maxdepth 1 -type f -iname 'README' -o -iname 'README.*' | sort | head -n1)"
        echo "${output:-$(get_project_dir)/README.md}"
    }

    # It runs lint
    workspace_lint_run_gitignore() {
        if [ ! -d "$(get_project_dir)/.git" ]; then
            return 0
        fi

        if [ ! -f "$1" ]; then
            echo "The .gitignore file is missing"
            return 1
        fi

        if [ ! -s "$1" ]; then
            echo "The .gitignore file is empty"
            return 1
        fi

        if [ "$(grep -cE '^\*$' "$1")" -eq 0 ]; then
            echo "The .gitignore file should have a special wildcard string * at the top of the file to ignore everything on default"
            return 1
        fi

        if [ "$(sed 's/[[:space:]]*#.*$//' "$1" | sed '/^[[:space:]]*$/d' | sed '/^\*$/q' | wc -l)" -ne 1 ]; then
            echo "The .gitignore file should have a special wildcard string * at the top of the file before everything else"
            return 1
        fi
    }

    # It runs lint
    workspace_lint_run_license() {
        if [ ! -f "$1" ]; then
            echo "The LICENSE file is missing"
            return 1
        fi

        if [ ! -s "$1" ]; then
            echo "The LICENSE file is empty"
            return 1
        fi
    }

    # It runs lint
    workspace_lint_run_readme() {
        if [ ! -f "$1" ]; then
            echo "The README file is missing"
            return 1
        fi

        if [ ! -s "$1" ]; then
            echo "The README file is empty"
            return 1
        fi
    }

    # It lints workspace files
    workspace_lint_run() {
        status=0

        if ! output="$(workspace_lint_run_gitignore "$(get_getignore_file)" 2>&1)"; then
            red ".gitignore issues for $(get_getignore_file)"
            echo "${output}"
            status=1
        fi

        if ! output="$(workspace_lint_run_readme "$(get_readme_file)" 2>&1)"; then
            red "README issues for $(get_readme_file)"
            echo "${output}"
            status=1
        fi

        if ! output="$(workspace_lint_run_license "$(get_license_file)" 2>&1)"; then
            red "LICENSE issues for $(get_license_file)"
            echo "${output}"
            status=1
        fi

        return "${status:-0}"
    }

    # It lints workspace files
    workspace_lint() {
        info "Running workspace lint..."

        if workspace_lint_run "$@"; then
            pass "Lint passed"
        else
            fail "Lint failed"
        fi
    }

    workspace_main() {
        if [ -n "${WORKSPACE_EXECUTED:-}" ]; then
            return
        fi

        WORKSPACE_FAILED=0

        case "$1" in
            lint)
                shift; workspace_lint "$@";;
            -*)
                exec git "$@";;
            "")
                ;;
            *)
                exec "$@";;
        esac

        export WORKSPACE_EXECUTED=1

        return "${WORKSPACE_FAILED:-0}"
    }

    workspace_main "$@"

.workspace:
    variables:
        WORKSPACE_DOCKER_IMAGE: registry.gitlab.com/tymonx/gitlab-ci/workspace
        WORKSPACE_DOCKER_TAG: 1.11.1
    image: ${WORKSPACE_DOCKER_IMAGE}:${WORKSPACE_DOCKER_TAG}
    needs: []
    rules:
        - if: $CI_COMMIT_TAG || $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_PIPELINE_SOURCE == "merge_request_event"
          when: always
        - when: never
    before_script:
        - set -- "${WORKSPACE_COMMAND:-}" ${WORKSPACE_ARGUMENTS:+${WORKSPACE_ARGUMENTS:-}}
        - *workspace-script
    script:
        - set -- "${WORKSPACE_COMMAND:-}" ${WORKSPACE_ARGUMENTS:+${WORKSPACE_ARGUMENTS:-}}
        - *workspace-script

.workspace-lint:
    extends: .workspace
    stage: test
    variables:
        WORKSPACE_COMMAND: lint
...
